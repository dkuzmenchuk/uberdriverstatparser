const request = require('request');

class Parser {
  constructor () {
    this.jar = request.jar();
    this.trips = [];
  }

  async parse () {
    let page = await this.openLoginPage();
    const csrf = this.extractCsrfToken(page);
    await this.verifyEmail(csrf);
    await this.verifyPassword(csrf);

    let json, host = 'https://partners.uber.com', path = '/p3/money/trips/?page=0';

    do {
      page = await this.openStatPage(host + path);
      json = this.parseStat(page);
      this.trips.push(...json.trips);
      path = json.nextUrl;
    } while (json.hasMore === true);

    // convert distance from ml to km
    this.trips.forEach(trip => {
      trip.distance *= 1.6087;
    });

    require('fs').writeFileSync('trips.json', JSON.stringify(this.trips));
    console.log('finish');
  }

  openLoginPage () {
    return new Promise((resolve, reject) => {
      request({
        uri: 'https://auth.uber.com/login',
        method: 'GET',
        jar: this.jar
      }, (error, response, body) => {
        error ? reject(error) : resolve(body);
      });
    });
  }

  extractCsrfToken (html) {
    const result = html.match(/window\.csrfToken = '(.+)';/);

    return result[1];
  }

  verifyEmail (csrf) {
    return new Promise((resolve, reject) => {
      request({
        uri: 'https://auth.uber.com/login/handleanswer',
        method: 'POST',
        jar: this.jar,
        json: true,
        gzip: true,
        headers: {
          'Host': 'auth.uber.com',
          'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0',
          'Accept': 'application/json',
          'Accept-Language': 'en-US,en;q=0.5',
          'Accept-Encoding': 'gzip, deflate, br',
          'x-csrf-token': csrf,
          'content-type': 'application/json',
          'Connection': 'keep-alive',
        },
        body: {
          answer: {
            type: 'VERIFY_INPUT_EMAIL',
            userIdentifier: {
              email: process.env.EMAIL
            }
          },
          init: true
        }
      }, (error, response, body) => {
        error ? reject(error) : resolve(body);
      });
    });
  }

  verifyPassword (csrf) {
    return new Promise((resolve, reject) => {
      request({
        uri: 'https://auth.uber.com/login/handleanswer',
        method: 'POST',
        jar: this.jar,
        json: true,
        gzip: true,
        headers: {
          'Accept':'application/json',
          'Accept-Encoding':'gzip, deflate, br',
          'Accept-Language':'en-US,en;q=0.9,ru;q=0.8',
          'Connection':'keep-alive',
          'content-type':'application/json',
          'Host':'auth.uber.com',
          'Origin':'https://auth.uber.com',
          'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0',
          'x-csrf-token': csrf
        },
        body: {
          answer: {
            type: 'VERIFY_PASSWORD',
            password: process.env.PASSWORD
          },
          rememberMe: true
        }
      }, (error, response, body) => {
        error ? reject(error) : resolve(body);
      });
    });
  }

  openStatPage (uri) {
    return new Promise((resolve, reject) => {
      request({
        uri: uri,
        method: 'GET',
        jar: this.jar,
        gzip: true,
        headers: {
          'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
          'Accept-Encoding':'gzip, deflate, br',
          'Accept-Language':'en-US,en;q=0.9,ru;q=0.8',
          'Connection':'keep-alive',
          'Host':'partners.uber.com',
          'Upgrade-Insecure-Requests':'1',
          'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0'
        }
      }, (error, response, body) => {
        error ? reject(error) : resolve(body);
      })
    })
  }

  parseStat (page) {
    const result = page.match(/window\.__JSON_GLOBALS_\["data"] = ({.+})\n/);
    const jsonString = result[1].replace(/new Date\((.*?)\),"/g, '$1,"');

    return JSON.parse(jsonString);
  }
}

module.exports = Parser;